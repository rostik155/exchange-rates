import React from 'react';
import {FallbackProps} from "react-error-boundary";

import "./styles/error-fallback.scss";

export const ErrorFallback = (props: FallbackProps) => {
  return <div className="error-message">
    <p>Connection error. Please, try again later.</p>
    <button onClick={props.resetErrorBoundary}>Retry</button>
  </div>
};
