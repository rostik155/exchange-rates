import React from 'react';
import {ExchangeRates} from "../ExchangeViews/ExchangeRates";
import {ServicesRegistry} from "../utils/ServicesRegistry";
import {appConfig} from "./appConfig";
import {Gitlab} from "../icons";
import {ErrorBoundary} from "react-error-boundary";
import {ErrorFallback} from "../Errors/ErrorFallback";

import "./styles/app.scss"

ServicesRegistry.init(appConfig);

const gitlabIcon = Gitlab({
  width: '80px',
});

function App() {
  return (
    <div className="app">
      <div className="container">
        <h3 className="pink-text text-darken-2">Currency exchange rates</h3>
        <ErrorBoundary FallbackComponent={ErrorFallback}>
          <ExchangeRates />
        </ErrorBoundary>
      </div>
      <footer>
        <a href="https://gitlab.com/rostik155/exchange-rates" target="_blank" rel="noreferrer">
          <span>Contribute to the project on </span>
          {gitlabIcon}
        </a>
      </footer>
    </div>
  );
}

export default App;
