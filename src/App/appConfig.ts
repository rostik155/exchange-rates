import {IExchangeRatesProvider} from "../CurrencyExchangeService/ExchangeRatesProvider";
import {OpenRatesProvider} from "../CurrencyExchangeService/OpenRatesProvider";
import {TCurrency} from "../ExchangeViews/currency";

export interface IAppConfig {
  readonly currencyRatesProvider: IExchangeRatesProvider,
  readonly defaultCurrency: TCurrency,
}

export const appConfig: IAppConfig = {
  currencyRatesProvider: new OpenRatesProvider(),
  defaultCurrency: 'EUR',
} as const;
