import {IExchangeRatesProviderProps} from "../CurrencyExchangeService/ExchangeRatesProvider";
import {ICurrencyRates} from "../CurrencyExchangeService";

export const MockedProvider = {
  fetch: jest.fn<Promise<ICurrencyRates>, [IExchangeRatesProviderProps]>(() => {
    throw new Error('Implementation mock is required');
  })
};