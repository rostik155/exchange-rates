import * as React from "react";

function SvgGitlab(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1266 559" {...props}>
      <style>
        {
          ".gitlab_svg__st3{fill:#e24329}.gitlab_svg__st4{fill:#fca326}.gitlab_svg__st5{fill:#fc6d26}.gitlab_svg__st6{fill:#8c929d}.gitlab_svg__st15{opacity:.1;fill:#e828e3}.gitlab_svg__st15,.gitlab_svg__st16{display:inline}.gitlab_svg__st18{font-family:&apos;SourceSansPro-Semibold&apos;}.gitlab_svg__st19{font-size:24px}"
        }
      </style>
      <g id="gitlab_svg__logo_art">
        <path
          id="gitlab_svg__path14_3_"
          className="gitlab_svg__st6"
          d="M839.7 198.192h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4z"
        />
        <g id="gitlab_svg__g24_3_" transform="translate(977.327 143.284)">
          <path
            id="gitlab_svg__path26_3_"
            className="gitlab_svg__st6"
            d="M13 188.892c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1H13zm-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9z"
          />
        </g>
        <g id="gitlab_svg__g28_3_" transform="translate(1099.767 143.129)">
          <path
            id="gitlab_svg__path30_3_"
            className="gitlab_svg__st6"
            d="M-17.7 201.192c-8.2 0-15.4-1-20.8-3.5v-75.1c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59"
          />
        </g>
        <g id="gitlab_svg__g32_5_" transform="translate(584.042 143.63)">
          <path
            id="gitlab_svg__path34_5_"
            className="gitlab_svg__st6"
            d="M18.3 72.192c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3c-12.7-11.2-29.9-17.2-48.3-17.2-46.4 0-78.9 28.3-78.9 85.4 0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9v-20.1H4v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6"
          />
        </g>
        <g id="gitlab_svg__g36_4_" transform="translate(793.569 142.577)">
          <path
            id="gitlab_svg__path38_4_"
            className="gitlab_svg__st6"
            d="M-37.7 55.592H-59l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5z"
          />
        </g>
        <path
          id="gitlab_svg__path40_33_"
          className="gitlab_svg__st6"
          d="M680.4 360.692h21.3v-124h-21.3v124z"
        />
        <path
          id="gitlab_svg__path42_3_"
          className="gitlab_svg__st6"
          d="M680.4 219.592h21.3v-21.3h-21.3v21.3z"
        />
        <g>
          <path
            id="gitlab_svg__path50_5_"
            className="gitlab_svg__st3"
            d="M292.778 434.892l62.199-191.322H230.669l62.109 191.322z"
          />
          <path
            id="gitlab_svg__path66_12_"
            className="gitlab_svg__st4"
            d="M143.549 243.57l-18.941 58.126c-1.714 5.278.137 11.104 4.661 14.394l163.509 118.801L143.549 243.57z"
          />
          <path
            id="gitlab_svg__path74_5_"
            className="gitlab_svg__st3"
            d="M143.549 243.57h87.12l-37.494-115.224c-1.919-5.895-10.282-5.895-12.27 0L143.549 243.57z"
          />
          <path
            id="gitlab_svg__path82_12_"
            className="gitlab_svg__st4"
            d="M442.097 243.57l18.873 58.126c1.714 5.278-.137 11.104-4.661 14.394L292.778 434.892 442.097 243.57z"
          />
          <path
            id="gitlab_svg__path86_5_"
            className="gitlab_svg__st3"
            d="M442.097 243.57h-87.12l37.425-115.224c1.919-5.895 10.282-5.895 12.27 0l37.425 115.224z"
          />
          <path
            className="gitlab_svg__st5"
            d="M292.778 434.892l62.199-191.322h87.12zM292.778 434.892L143.549 243.57h87.12z"
          />
        </g>
      </g>
    </svg>
  );
}

export default SvgGitlab;
