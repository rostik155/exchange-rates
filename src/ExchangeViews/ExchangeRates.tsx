import {ALL_CURRENCIES, TCurrency} from "./currency";
import {IExchangeRate, ICurrencyRates} from "../CurrencyExchangeService";
import {ChangeEvent, MouseEvent, useCallback, useEffect, useState} from "react";
import {ExchangeChart} from "./ExchangeChart";
import {ServicesRegistry} from "../utils/ServicesRegistry";
import LoadingHOC from "../utils/LoadingHOC";

import "./styles/exchange-rates.scss";
import {useFetchErrorHandler} from "../utils/hooks";
import {appConfig} from "../App/appConfig";

interface IExchangeRatesProps {
  startLoading: () => void,
  stopLoading: () => void,
}

const ExchangeRatesBase = (props: IExchangeRatesProps) => {
  const [baseCurrency, setBaseCurrency] = useState<TCurrency>(appConfig.defaultCurrency);
  const [exchangeCurrency, setExchangeCurrency] = useState<TCurrency|undefined>();
  const [exchangeRates, setExchangeRates] = useState<IExchangeRate[]>([]);
  const handleError = useFetchErrorHandler();
  const { currencyRatesService } = ServicesRegistry;
  const { startLoading, stopLoading } = props;

  useEffect(() => {
    startLoading();
    currencyRatesService.fetch(baseCurrency).then((rates: ICurrencyRates) => {
      setExchangeRates(rates.currencyRates);
      stopLoading();
    }).catch(handleError);
    return () => {
      currencyRatesService.abort();
    }
    // currencyRatesService is not expected to change
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [baseCurrency, startLoading, stopLoading]);

  const onBaseCurrencySet = useCallback((e: ChangeEvent<HTMLSelectElement>) => {
    setBaseCurrency(e.target.value as TCurrency);
  }, []);

  const onExchangeCurrencySet = useCallback((e: MouseEvent<HTMLDivElement>) => {
    const target = e.currentTarget;
    setExchangeCurrency(target.dataset.currency as TCurrency);
  }, []);

  return <>
    <div className="col s12 select-container">
      <label>Select base currency:</label>
      <select value={baseCurrency} onChange={onBaseCurrencySet} className="browser-default">
        {ALL_CURRENCIES.map((currency: TCurrency): JSX.Element =>
          <option value={currency} key={currency}>{currency}</option>)}
      </select>
    </div>
    <div className="rates">
    {exchangeRates.map((exchangeRate: IExchangeRate) => {
      const { name, rate } = exchangeRate;
      const className = ['rates-child']
      if (name === exchangeCurrency) {
        className.push('active');
      }
      return <div key={name} data-currency={name} className={className.join(' ')} onClick={onExchangeCurrencySet}>
        <div><b>{name}</b></div>
        <div>{rate}</div>
      </div>
    })}
    </div>
    <ExchangeChart baseCurrency={baseCurrency} exchangeCurrency={exchangeCurrency} />
  </>
};

export const ExchangeRates = LoadingHOC(ExchangeRatesBase);
