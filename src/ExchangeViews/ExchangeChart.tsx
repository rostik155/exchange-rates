import {TCurrency} from "./currency";
import {useEffect, useMemo, useState} from "react";
import {ICurrencyRates} from "../CurrencyExchangeService";
import 'c3/c3.css';
import {ServicesRegistry} from "../utils/ServicesRegistry";
import LoadingHOC, {WithLoadingProps} from "../utils/LoadingHOC";
import {useFetchErrorHandler} from "../utils/hooks";

// @ts-ignore
import C3Chart from 'react-c3js';

import "./styles/exchange-chart.scss";

interface IBaseExchangeChartProps {
  baseCurrency: TCurrency;
  exchangeCurrency?: TCurrency;
}

type TExchangeChartProps = IBaseExchangeChartProps & WithLoadingProps;

interface ChartData {
  labels: string[];
  values: number[];
  min: number;
  max: number;
}

const getDefaultChartData = (): ChartData => ({ labels: [], values: [], min: 0, max: 0 });

const reduceDays = (date: Date, days: number): Date => {
  const result = new Date(date);
  result.setDate(result.getDate() - days);
  return result;
}

// Define constants to avoid "magic numbers"
const WEEK_DAYS_COUNT = 7 as const;
const WEEKEND_DAYS_COUNT = 2 as const;
const WEEKEND_DAYS: number[] = [6, 0]; // Saturday and Sunday

const BaseCurrencyChart = (props: TExchangeChartProps) => {
  const {baseCurrency, exchangeCurrency, startLoading, stopLoading} = props;
  const [chartData, setChartData] = useState<ChartData>(getDefaultChartData());
  const handleError = useFetchErrorHandler();
  const { currencyRatesService } = ServicesRegistry;

  useEffect(() => {
    if (!exchangeCurrency) {
      setChartData(getDefaultChartData());
      return;
    }
    const now = new Date();
    const promises: Promise<ICurrencyRates>[] = [];

    // Sending requests
    startLoading();
    for (let i = WEEK_DAYS_COUNT + WEEKEND_DAYS_COUNT - 1; i >= 0; i--) {
      const date = reduceDays(now, i);
      if (!WEEKEND_DAYS.includes(date.getDay())) { // no data for weekend days
        promises.push(currencyRatesService.fetch(baseCurrency, exchangeCurrency, date));
      }
    }

    // Waiting for responses
    Promise.all(promises).then((allData: ICurrencyRates[]) => {
      if (!exchangeCurrency) {
        return;
      }
      const newWeeklyRates: ChartData = getDefaultChartData();
      allData.forEach(((data) => {
        newWeeklyRates.labels.push(data.date);
        newWeeklyRates.values.push(data.currencyRates[0].rate);
      }));
      newWeeklyRates.min = Math.min(...newWeeklyRates.values);
      newWeeklyRates.max = Math.max(...newWeeklyRates.values);
      setChartData(newWeeklyRates);
      stopLoading();
    }).catch(handleError);

    // Aborting old requests before requesting new ones
    return () => {
      currencyRatesService.abort();
    }
    // currencyRatesService is not expected to change
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [baseCurrency, exchangeCurrency, startLoading, stopLoading])

  const chartProps = useMemo(() => {
    return {
      data: {
        x: 'x',
        columns: [
          ['x', ...chartData.labels],
          ['Exchange rate', ...chartData.values],
        ],
        colors: {
          'Exchange rate': '#c2185b'
        }
      },
      axis: {
        x: {
          type: 'category',
        },
        y: {
          tick: {
            format: (x: number) => parseFloat(x.toFixed(10)),
          }
        }
      },
      legend: {show: false},
    }
  }, [chartData]);

  if (!props.exchangeCurrency) return null;

  return <>
    <h5>{baseCurrency} to {exchangeCurrency} exchange rates for last week:</h5>
    <C3Chart {...chartProps} />
    <div className="chart-footer">
      <div>Min exchange rate: <strong>{chartData.min}</strong></div>
      <div>Max exchange rate: <strong>{chartData.max}</strong></div>
    </div>
  </>;
};

export const ExchangeChart = LoadingHOC<IBaseExchangeChartProps>(BaseCurrencyChart);
