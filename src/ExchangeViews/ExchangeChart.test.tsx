import React from 'react';
import {render, waitFor} from '@testing-library/react';
import {ServicesRegistry} from "../utils/ServicesRegistry";
import {MockedProvider} from "../test-utils/MockedProvider";
import {ICurrencyRates} from "../CurrencyExchangeService";
import {TCurrency} from "./currency";
import {ExchangeChart} from "./ExchangeChart";
// @ts-ignore
import C3Chart from 'react-c3js';

jest.mock('react-c3js')

ServicesRegistry.init({
  defaultCurrency: 'EUR',
  currencyRatesProvider: MockedProvider,
});

const createResponse = (name: TCurrency, rate: number, date: string): Promise<ICurrencyRates> => {
  return Promise.resolve({currencyRates: [{name, rate}], date});
}

describe('Testing ExchangeChart view component', () => {
  it('Displays correct min and max values', async () => {
    C3Chart.mockClear();

    MockedProvider.fetch
      .mockImplementationOnce(() => createResponse('USD', 1.39, '2021-01-01'))
      .mockImplementationOnce(() => createResponse('USD', 1.41, '2021-01-02'))
      .mockImplementationOnce(() => createResponse('USD', 1.44, '2021-01-03'))
      .mockImplementationOnce(() => createResponse('USD', 1.45, '2021-01-04'))
      .mockImplementationOnce(() => createResponse('USD', 1.42, '2021-01-05'))
      .mockImplementationOnce(() => createResponse('USD', 1.38, '2021-01-06'))
      .mockImplementationOnce(() => createResponse('USD', 1.36, '2021-01-07'));

    const {container} = render(<ExchangeChart baseCurrency="GBP" exchangeCurrency="USD" />);
    const chartFooterEls = container.querySelectorAll('.chart-footer div strong');
    const minEl = chartFooterEls[0];
    const maxEl = chartFooterEls[1];
    await waitFor(() => expect(minEl?.textContent).toBe('1.36'));
    await waitFor(() => expect(maxEl?.textContent).toBe('1.45'));
  });
});
