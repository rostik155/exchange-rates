import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { ExchangeRates } from './ExchangeRates';
import {ServicesRegistry} from "../utils/ServicesRegistry";
import {MockedProvider} from "../test-utils/MockedProvider";
import {IExchangeRatesProviderProps} from "../CurrencyExchangeService/ExchangeRatesProvider";
import {ICurrencyRates} from "../CurrencyExchangeService";

ServicesRegistry.init({
  defaultCurrency: 'EUR',
  currencyRatesProvider: MockedProvider,
});

describe('Testing ExchangeRates view component', () => {
  beforeEach(() => {
    MockedProvider.fetch.mockImplementation((props: IExchangeRatesProviderProps): Promise<ICurrencyRates> => {
      if (props.baseCurrency === 'EUR' && !props.exchangeCurrency) {
        return Promise.resolve({
          currencyRates: [
            {name: 'GBP', rate: 0.85},
            {name: 'USD', rate: 1.2},
            {name: 'RUB', rate: 90.5},
          ],
          date: '2021-01-01',
        });
      }
      throw new Error('Unexpected arguments');
    });
  });

  it('uses default currency from app configuration', async () => {
    const { container } = render(<ExchangeRates />);
    const selectEl = container.getElementsByTagName('select')?.[0];
    await waitFor(() => expect(selectEl?.value).toBe('EUR'));
  });

  it('displays received exchange rates', async () => {
    const { container } = render(<ExchangeRates />);
    const ratesEls = container.getElementsByClassName('rates-child');
    await waitFor(() => expect(ratesEls.length).toBe(3));

    const currency = ratesEls[0].getElementsByTagName('b')?.[0];
    await waitFor(() => expect(currency?.innerHTML).toBe('GBP'));

    const rate = ratesEls[0].getElementsByTagName('div')?.[1];
    await waitFor(() => expect(rate?.innerHTML).toBe('0.85'));
  });
});
