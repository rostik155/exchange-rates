import {TCurrency} from "../ExchangeViews/currency";
import {ICurrencyRates} from "./";

export interface IExchangeRatesProviderProps {
  signal: AbortSignal,
  baseCurrency: TCurrency
  exchangeCurrency?: TCurrency
  when?: Date
}

export interface IExchangeRatesProvider {
  fetch: (props: IExchangeRatesProviderProps) => Promise<ICurrencyRates>;
}
