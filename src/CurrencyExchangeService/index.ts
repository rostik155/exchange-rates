import {TCurrency} from "../ExchangeViews/currency";
import {IExchangeRatesProvider} from "./ExchangeRatesProvider";

export interface IExchangeRate {
  name: TCurrency;
  rate: number;
}

export interface ICurrencyRates {
  currencyRates: IExchangeRate[];
  date: string;
  base?: TCurrency;
}

export class ExchangeRatesService {
  private fetchAbortController: AbortController;

  constructor(private provider: IExchangeRatesProvider) {
    this.fetchAbortController = new AbortController();
  }

  async fetch(baseCurrency: TCurrency, exchangeCurrency?: TCurrency, when?: Date): Promise<ICurrencyRates> {
    if (this.fetchAbortController.signal.aborted) {
      this.fetchAbortController = new AbortController();
    }
    return this.provider.fetch({
      signal: this.fetchAbortController.signal,
      baseCurrency,
      exchangeCurrency,
      when
    });
  }

  abort() {
    this.fetchAbortController.abort()
  }
}
