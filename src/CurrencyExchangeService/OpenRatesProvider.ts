import {TCurrency} from "../ExchangeViews/currency";
import {ICurrencyRates} from "./";
import {IExchangeRatesProvider, IExchangeRatesProviderProps} from "./ExchangeRatesProvider";

type TOriginalRates = Record<TCurrency, number>;

interface IOriginalData {
  rates: TOriginalRates;
  date: string;
  base?: TCurrency;
}

export class OpenRatesProvider implements IExchangeRatesProvider {
  getUrl(baseCurrency: TCurrency, exchangeCurrency?: TCurrency, when?: Date): string {
    let url = `https://api.openrates.io/`;
    if (when) {
      url += `${when.getFullYear()}-${when.getMonth() + 1}-${when.getDate()}`;
    } else {
      url += 'latest';
    }
    url += `?base=${baseCurrency}`;
    if (exchangeCurrency) {
      url += `&symbols=${exchangeCurrency}`;
    }
    return url;
  }

  async fetch(props: IExchangeRatesProviderProps): Promise<ICurrencyRates> {
    const {signal, baseCurrency, exchangeCurrency, when} = props;

    const response: Response = await fetch(this.getUrl(baseCurrency, exchangeCurrency, when), { signal });
    const data = await response.json() as IOriginalData;
    return this.format(data);
  }

  format(data: IOriginalData): ICurrencyRates {
    const currencies: TCurrency[] = Object.keys(data.rates) as TCurrency[];
    const currencyRates = currencies.map((name) => {
      return {
        name,
        rate: data.rates[name],
      };
    });
    return {
      currencyRates,
      date: data.date,
      base: data.base,
    }
  }
}