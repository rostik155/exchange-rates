import {useCallback, useState} from "react";

import "./styles/loading.scss";

export interface WithLoadingProps {
  startLoading: () => void,
  stopLoading: () => void,
}

export default function LoadingHOC <P extends Object>(
  ComponentToWrap: React.ComponentType<P & WithLoadingProps>
): React.ComponentType<P> {

  const WrappedComponent = (props: P) => {
    const [isLoading, setLoadingState] = useState<boolean>(false);

    const startLoading = useCallback(() => setLoadingState(true), []);
    const stopLoading = useCallback(() => setLoadingState(false), []);

    const className = ['loading-container'];
    if (isLoading) {
      className.push('loading');
    }
    return <div className={className.join(' ')}>
      <div className="loader-screen">Loading...</div>
      <ComponentToWrap {...props} startLoading={startLoading} stopLoading={stopLoading} />
    </div>;
  }

  return WrappedComponent;
}
