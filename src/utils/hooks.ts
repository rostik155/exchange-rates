import {useErrorHandler} from "react-error-boundary";

export const useFetchErrorHandler = () => {
  const handleError = useErrorHandler();

  return (givenError: Error | null) => {
    if (givenError?.name === "AbortError") {
      return;
    }
    handleError(givenError);
  };
}