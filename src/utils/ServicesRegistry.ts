import {IAppConfig} from "../App/appConfig";
import {ExchangeRatesService} from "../CurrencyExchangeService";


export class ServicesRegistry {
  private static services: {
    currencyRatesService?: ExchangeRatesService
  } = {};

  static init(appConfig: IAppConfig) {
    ServicesRegistry.services.currencyRatesService = new ExchangeRatesService(appConfig.currencyRatesProvider);
  }

  static get currencyRatesService(): ExchangeRatesService {
    if (ServicesRegistry.services?.currencyRatesService === undefined) {
      throw new Error('CurrencyExchangeService is not initialized');
    }
    return ServicesRegistry.services.currencyRatesService;
  }
}