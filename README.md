# General info

**Exchange currency rates app**

Live: https://exchange-rates.gl.rostik.pro/

## Tools required

 - NodeJs 15+
 - Yarn (LTS is recommended)

## Browsers compatibility

  - Edge 90+
  - Firefox 63+
  - Chrome 84+
  - Safari 14.1+
  - Opera 73+
  - Safari on IOS 14.5+
  - Android Browser 90+
  - Chrome for Android 90+

## Libraries used

  - [Create React App](https://github.com/facebook/create-react-app) for bootstrapping
  - [C3.js](https://c3js.org/) + [react-c3js](https://github.com/bcbcarl/react-c3js) for charts
  - [Materialize](https://materializecss.com/) for styling
  - [node-sass](https://github.com/sass/node-sass/) for scss support
  - [react-svgr](https://react-svgr.com/) for transforming SVGs into React components
  - [Jest](https://jestjs.io/) for unit testing
  - Other usual for react app tools (see [package.json](./package.json))

## Running locally

``` bash
yarn start
```
